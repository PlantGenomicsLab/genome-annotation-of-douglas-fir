### Process for Final Braker Annotation (Filtering)

## Initial Files: 

	Preliminary Braker Annotation: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation/all_scaffolds/gfacs/filtered/braker_wlr__out.gtf
	
## Process of Filtering Braker Annotation

All scripts can be found in ./scripts

1. Overlap analysis was performed on the annotation. This was accomplished using bedtools and any time a gene (or multiple genes) aligned to a longer transcript, the transcript was used to replace the gene in the annotation.

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/1_overlap_analysis

	gFACs here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/gfacs/intermediate_filters/bedtools_filtered/

2. Genes which aligned to sequences > 80% softmasked in the genome were removed from the annotation. This was accomplished using the scripts remove_softmasked.py and removesoftmasked.sh. 

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/2_remove_softmasked

	gFACs here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/gfacs/intermediate_filters/soft_bedtools_filtered

3. Genes were filtered based on the presence of a functional annotation. Genes without a functional annotation based on similiarity search (query/coverage 50/50) were removed from the annotation. .

        Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/3_functional_annotation

	gFACs here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/gfacs/intermediate_filters/

4. Interproscan was run on the resulting gene set to annotate sequences as retrotransposons and retrodomains. In order to run interproscan the format of the transcriptome file was altered using the script format.sh Interproscan was run using the script interproscan.sh .

        Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/4_interpro 

5. Monoexonic genes were further filtered based on annotation via similiarity search at the 80/80 level.

	Monoexonic results located here:  /core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/gfacs/other_filtering_attempts/mono_8080_func_soft_bedtools_filtered/mono_braker_wlr_bedtools_soft_func_out.gtf

	Monoexonic gFACs here: /core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/gfacs/other_filtering_attempts/mono_8080_func_soft_bedtools_filtered/mono_braker_wlr_bedtools_soft_func_statistics.txt

	Multiexonic results located here:/core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/gfacs/other_filtering_attempts/multi_8080_func_soft_bedtools_filtered/multi_braker_wlr_bedtools_soft_func_out.gtf

	Multiexonic gFACs here:/core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/gfacs/other_filtering_attempts/multi_8080_func_soft_bedtools_filtered/multi_braker_wlr_bedtools_soft_func_statistics.txt

5. The annotation was filtered based on the interproscan results. Genes which were labeled as "gag-polypeptide", "retrotransposon", "reverse transcriptase", "retrovirus", "copia", or "gypsy" were removed from the annotation. Monoexonic genes were additionally filtered to remove any retrodomains, as denoted by Pfam. This was accomplished by using the filter_mono_interpro.py and filter_multi_interpro.py scripts.

	Results located here: 

	gFACs here: 


## Assessing Final Annotation Quality

1. gFACs statistics on the resulting annotation were generated using the script final_gFACs.sh 

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/gfacs/final_annotation

2. BUSCO completeness score generated (on filtered annotation) by script busco.sh

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/busco/final_annotation

Results Summary:

        C:69.1%[S:58.5%,D:10.6%],F:11.5%,M:19.4%,n:1614
        1115    Complete BUSCOs (C)
        944     Complete and single-copy BUSCOs (S)
        171     Complete and duplicated BUSCOs (D)
        185     Fragmented BUSCOs (F)
        314     Missing BUSCOs (M)
        1614    Total BUSCO groups searched

3. Functional annotation using ENTAP, aligining to two databases, at the 50/50 and 80/80 query coverage

Databases used:

	/labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd
	/isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd 

Results located here:

	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/final_set_entap_5050
	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/final_set_entap_8080

4. A table of the distribution of intron_lengths was made with the script intron_length.py and intron_length.sh and a table of gene lengths was made with the length.py and length.sh.

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered/refined_violin_length
