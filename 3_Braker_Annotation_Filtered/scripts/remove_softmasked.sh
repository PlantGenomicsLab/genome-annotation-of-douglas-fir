#!/bin/bash
#SBATCH --job-name=morethan1.py
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=alyssa.ferreira@uconn.edu
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err
#SBATCH --qos=general

module load bedtools
module load biopython/1.70
module load anaconda2/2.4.1
python2 remove_softmasked.py

