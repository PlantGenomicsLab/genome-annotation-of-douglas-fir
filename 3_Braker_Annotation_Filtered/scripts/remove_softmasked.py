import numpy as np
import pandas as pd
import subprocess
import glob, os
from Bio import SeqIO

inputGFF = pd.read_table("../gfacs/bedtools_filtered/braker_wlr_bedtools__out.gtf", sep='\t', names=["scafnum", "source", "type", "start", "end", "score", "strand", "phase", "id", "freq"])
v = pd.read_table("../gfacs/bedtools_filtered/braker_wlr_bedtools__out.gtf", sep='\t', names=["scafnum", "source", "type", "start", "end", "score", "strand", "phase", "id", "freq"])
inputGFF = inputGFF[inputGFF['type']=='gene']
inputGFF = inputGFF[["scafnum", "start", "end", 'id']]
inputGFF.to_csv('intermediate.gff', sep='\t', index=False, header=False) 
with open('bed_intermediatey.bed', "w") as outfile:
	subprocess.call(['bedtools','getfasta','-fi','/isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0.5000/Psme_v1.0.5000.genome.masked.fa','-bed','intermediate.gff','-name'],stdout=outfile)



column_names = ["id", "soft", "len"]
c = pd.DataFrame(columns = column_names)
for record in SeqIO.parse("bed_intermediatey.bed", "fasta"):
	seq = record.seq
	n = seq.count('a') + seq.count('c') + seq.count('t') + seq.count('g')
	l = len(record)
	z = (float(n))/(float(l))
	p = record.id
	c = c.append({'id': p, 'soft':z, 'len':l} , ignore_index=True)

z = v.join(c.set_index('id'), on='id')
z.to_csv('output.csv',  sep='\t', index=False, header=False)
z['soft'] = z['soft'].fillna(method='ffill')
z = z[z['soft'] < .8]
z.to_csv('test.gff',  sep='\t', index=False, header=False)
z=z[["scafnum", "source", "type", "start", "end", "score", "strand", "phase", "id"]]
z.to_csv('final_filtered_entap_soft.gff',  sep='\t', index=False, header=False)








'''
seqlist = pd.read_table("bed_intermediate.bed",  sep='\t', names =['id', 'seq'])
z = pd.merge(v, seqlist, on='id')
z['length'] = z['stop'] - z['start']
z['little_a'] = z.str.count('a')
z['little_c'] = z.str.count('c')
z['little_t'] = z.str.count('t')
z['little_g'] = z.str.count('g')
z['soft'] = (z['little_a'] + z['little_c'] + z['little_t'] + z['little_g'])/(z['length'])
inputGFF = inputGFF[["scafnum", "start", "end", 'id']]
z=z[["scafnum", "source", "type", "start", "end", "score", "strand", "phase", "id", "soft"]]
z.to_csv('output.csv',  sep='\t', index=False, header=False)
for record in SeqIO.parse("/isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0.5000/Psme_v1.0.5000.masked.fasta", "fasta"):
        seq = record.seq
        n = seq.count('N')
        z = n/(len(record))
        print(z)
'''
