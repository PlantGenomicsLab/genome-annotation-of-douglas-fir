### Process for Analyzing Published Maker Annotation:

## Original Annotation:

File Location:

	/isg/treegenes/treegenes_store/FTP/Genomes/Psme/v1.0/annotation/v1.0
	
Link to Publication:

	https://www.g3journal.org/content/7/9/3157	

Analysis performed here: 

	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation	

## Assessing Annotation Quality

All scripts can be found in /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation/scripts
Numbers may be different from the publication because of updated software versions

1. gFACs statistics on the initial annotation were generated using the script gFACs_unfiltered.sh

        Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation/gfacs/unfiltered

2. gFACs was used to filter the annotation using the script gFACs_filtered.sh with the following parameters

        --unique-genes-only \
	--no-processing \

        Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation/gfacs/filtered

2. BUSCO completeness score generated (on filtered annotation) by script busco.sh

        Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation/busco

Results Summary:

	C:29.4%[S:24.4%,D:5.0%],F:15.4%,M:55.2%,n:1614
        475     Complete BUSCOs (C)
        394     Complete and single-copy BUSCOs (S)
        81      Complete and duplicated BUSCOs (D)
        248     Fragmented BUSCOs (F)
        891     Missing BUSCOs (M)
        1614    Total BUSCO groups searched

3. Functional annotation using ENTAP, aligining to two databases, at the 50/50 and 80/80 query coverage

Databases used:

        /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd
        /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd

Results located here:

	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation/entap_5050
	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation/entap_8080

4. A table of the distribution of intron_lengths was made with the script intron_length.py and intron_length.sh and a table of gene lengths was made with the length.py and length.sh.

        Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation/violin_length

