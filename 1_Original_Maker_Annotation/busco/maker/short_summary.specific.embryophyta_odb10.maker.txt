# BUSCO version is: 4.0.2 
# The lineage dataset is: embryophyta_odb10 (Creation date: 2019-11-20, number of species: 50, number of BUSCOs: 1614)
# Summarized benchmarking in BUSCO notation for file /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/analyze_orig_maker/gfacs/filtered/maker_genes.fasta.faa
# BUSCO was run in mode: proteins

	***** Results: *****

	C:29.4%[S:24.4%,D:5.0%],F:15.4%,M:55.2%,n:1614	   
	475	Complete BUSCOs (C)			   
	394	Complete and single-copy BUSCOs (S)	   
	81	Complete and duplicated BUSCOs (D)	   
	248	Fragmented BUSCOs (F)			   
	891	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   
