#!/bin/bash
#SBATCH --job-name=maker_unfiltered
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=email@uconn.edu
#SBATCH -o gfacs_maker_%j.o
#SBATCH -e gfacs_maker_%j.e
#SBATCH --qos=general

module load perl/5.24.0
cd /labs/Wegrzyn/gFACs/
perl gFACs.pl \
-f maker_2.31.9_gff \
--unique-genes-only \
--statistics \
-p maker \
--create-gtf \
--no-processing \
--get-fasta-without-introns \
--get-protein-fasta \
--create-gtf \
--fasta /isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0.5000/Psme_v1.0.5000.genome.masked.fa \
-O /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/analyze_orig_maker/gfacs/filtered/ \
/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/analyze_orig_maker/Psme.allgenes.gff3

