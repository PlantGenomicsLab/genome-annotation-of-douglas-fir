#!/bin/bash
#SBATCH --job-name=entap_t5050
#SBATCH --mail-user=alyssa.ferreira@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 20
#SBATCH --mem=70G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=himem
#SBATCH --qos=himem

module load anaconda/4.4.0
module load perl/5.28.1
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/EnTAP --paths /labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/entap_config.txt --runP -d /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd -i ../gfacs/filtered/maker_genes.fasta.faa --out-dir /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/analyze_orig_maker/entap_5050 --qcoverage 50 --tcoverage 50 --taxon pseudotsuga --threads 20 --ontology 0
