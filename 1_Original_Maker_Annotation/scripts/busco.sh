#!/bin/bash
#SBATCH --job-name=busco_maker_filtered
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=32
#SBATCH --partition=general
#SBATCH --mail-type=END
####SBATCH --mem=1G
#SBATCH --mail-user=alyssa.ferreira@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err
#SBATCH --qos=general

module load busco/4.0.2
module unload augustus
export PATH=/home/CAM/aferreira/3.2.3/bin:/home/CAM/aferreira/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/3.2.3/config


busco -i /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/analyze_orig_maker/gfacs/filtered/maker_genes.fasta.faa -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o maker -m prot -c 32
