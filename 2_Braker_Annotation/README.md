### Process for Braker Annotation (pre-filtering)

## Initial Files: 

Unpaired short reads:

	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/UCHC/LABS/Wegrzyn/ConiferGenomes/Psme/analysis/raw_reads/trimmed/

Paired short reads:
	
	/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/3_Quality_control/Trimmomatic/trim_NS.1157.004.NEBNext_dual_i7_E3---NEBNext_dual_i5_E3.GC1-63L-NC*.fastq.gz
	/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/3_Quality_control/Trimmomatic/trim_NS.1157.004.NEBNext_dual_i7_F3---NEBNext_dual_i5_F3.GC9-67M-NHW*.fastq.gz
	/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/3_Quality_control/Trimmomatic/trim_NS.1165.001.NEBNext_dual_i7_E6---NEBNext_dual_i5_E6.GC5-52L-NHW*.fastq.gz
	/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/3_Quality_control/Trimmomatic/trim_NS.1170.003.NEBNext_dual_i7_H3---NEBNext_dual_i5_H3.GC7-65T-NC*.fastq.gz

Long read reference transcriptome:
	/labs/Wegrzyn/CoAdapTree_Douglasfir/Assembly_comparison/01_De_novo_transcriptome/02_Vsearch/all.95.centroids.80.centroids.uniq.pep.fasta


## Annotation Process

All scripts can be found in ./scripts

The process was run on other_scaffolds (approx 75,000 scaffolds) independently of 150 missing scaffolds


1. Genometools.sh was used to prepare the long read transcripts for genome threader. Genomethreader.sh was used to run genomethreader.

	Results here:

2. hisat2 was used to align paired and unpaired short reads to the genome:

3. Braker was run using the hisat alignments and genomethreader results as training evidence. The script braker.sh was used

for the 150 scaffolds whih were left out:

1. augustus was run reliant on previous braker run training

2. results were merged with the larger braker run.




## Assessing Genome Annotation Quality

1. gFACs statistics on the initial annotation were generated using the script unfiltered_gFACs.sh 

	Results located here: /core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation/all_scaffolds/gfacs/unfiltered/braker_wlr__statistics.txt

2. gFACs was used to filter the annotation using the script filtered_gFACs.sh with the following parameters

	--unique-genes-only \
	--min-CDS-size 300 \
	--rem-genes-without-start-and-stop-codon \
	--allowed-inframe-stop-codons 0 \
	--min-exon-size 9 \
	--min-intron-size 9 \

	Results located here: /core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation/all_scaffolds/gfacs/filtered/braker_wlr__statistics.txt

2. BUSCO completeness score generated (on filtered annotation) by script busco.sh

	Results located here:/core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation/all_scaffolds/busco/filtered/braker_wlr/short_summary.specific.embryophyta_odb10.braker_wlr.txt


Results Summary:

	C:64.6%[S:41.6%,D:23.0%],F:5.6%,M:29.8%,n:1614
        1043    Complete BUSCOs (C)
        672     Complete and single-copy BUSCOs (S)
        371     Complete and duplicated BUSCOs (D)
        91      Fragmented BUSCOs (F)
        480     Missing BUSCOs (M)
        1614    Total BUSCO groups searched

3. Functional annotation using ENTAP, aligining to two databases, at the 50/50 and 80/80 query coverage

Databases used:

	/labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd
	/isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd 

	Results located here for 50/50:/core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation/all_scaffolds/entap_5050/final_results/
	Results located here for 80/80:/core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation/all_scaffolds/entap_8080/final_results/

4. A table of the distribution of intron_lengths was made with the script intron_length.py and intron_length.sh and a table of gene lengths was made with the length.py and length.sh.

	Results located here for intron length:/core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation/all_scaffolds/violin_length/BrakerLongRead4ViolinIntron.csv
	Results located here for gene length: /core/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation/all_scaffolds/violin_length/BrakerLongRead4Violin.csv
