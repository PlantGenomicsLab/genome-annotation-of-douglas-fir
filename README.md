### Process for Douglas Fir Genome Annotation Improvement

## Location on Xanadu:
	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final

## Original Publication Link: 
	https://www.g3journal.org/content/7/9/3157

## Process of Genome Annotation Improvement

Several different genome annotation models were compared and utilized to produce a final improved genome annotation. The original genome annotation publised previously and produced using Maker was compared to the new models. Using Illumina paired and unpaired short reads and Pac-Bio Iso-Seq long-reads a novel transcriptome alignment (gth) was produced. A new ab initio annotation was generated using Braker. This was filtered dependent on functional annotation, repetitiveness of genes, overlap with the transcriptome alignment and retrotransposon removal. 

## For More on Each Annotation Process and Analysis:

	Maker: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment
	Aligned Transcriptome: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/1_Original_Maker_Annotation
	Braker: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/2_Braker_Annotation
	Filtered Braker: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/3_Braker_Annotation_Filtered

## Quality of Final Annotation: summary gFACs

	Total Genes	53506
	5’ Partial	0
	3’ Partial	0
	Average Length	21065.54
	Median Length	2049
	Multi-exonics	43654
	Mono-exonics	9852
	Longest intron	778429
	Average number of exons per multiexonic gene	4.867
	Functional annotation (SSS 50/50) %	100%

## Quality of Final Annotation: BUSCO Score

        C:69.1%[S:58.5%,D:10.6%],F:11.5%,M:19.4%,n:1614
        1115    Complete BUSCOs (C)
        944     Complete and single-copy BUSCOs (S)
        171     Complete and duplicated BUSCOs (D)
        185     Fragmented BUSCOs (F)
        314     Missing BUSCOs (M)
        1614    Total BUSCO groups searched




