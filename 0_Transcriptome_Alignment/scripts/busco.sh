#!/bin/bash
#SBATCH --job-name=busco_gmap_filt
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --partition=himem
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=alyssa.ferreira@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err
#SBATCH --qos=himem

module load busco/4.0.2
module unload augustus
export PATH=/home/CAM/aferreira/3.2.3/bin:/home/CAM/aferreira/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/3.2.3/config

busco -i /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/23_Fixed_Gmap/gFACs/filtered_in_frame_stop/transcriptome_genes.fasta.faa -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o transcriptome  -m prot -c 32

