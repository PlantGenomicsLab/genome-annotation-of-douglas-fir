#!/bin/bash
#SBATCH --job-name=gmap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=alyssa.ferreira@uconn.edu
#SBATCH -o gmap_%j.out
#SBATCH -e gmap_%j.err
#SBATCH --qos=general

module load gmap
#for CONIFERS ONLY

gmapl -K 1000000 -L 10000000 --cross-species -D /isg/shared/databases/alignerIndex/plant/Psme/gmap_2019/  -d psme_5000bps -f gff3_gene ../../18_Updated_GMAP/vsearch/centroids_combined_cat_df_trans.fasta  --fulllength --nthreads=16 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 -T > updated_gmap_genes_psme.gff3 2> query95_1_updated_95_95.error




