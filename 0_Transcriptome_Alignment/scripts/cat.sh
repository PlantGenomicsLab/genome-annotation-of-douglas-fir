#!/bin/bash
#SBATCH --job-name=copier
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=alyssa.ferreira@uconn.edu
#SBATCH -o cat_%j.out
#SBATCH -e cat_%j.err
#SBATCH --qos=general
#SBATCH --qos=general

cat /labs/Wegrzyn/ConiferGenomes/Psme/analysis/Genes/Illumina/illumina_transcrips.fasta /labs/Wegrzyn/CoAdapTree_Douglasfir/Assembly_comparison/01_De_novo_transcriptome/02_Vsearch/all.95.centroids.80.centroids.uniq.pep.fasta /labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly/trinity_trim_NS.1157.004.NEBNext_dual_i7_E3---NEBNext_dual_i5_E3.GC1-63L-NC.Trinity.fasta /labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly/trinity_trim_NS.1170.003.NEBNext_dual_i7_H3---NEBNext_dual_i5_H3.GC7-65T-NC.Trinity.fasta /labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly/trinity_trim_NS.1165.001.NEBNext_dual_i7_E6---NEBNext_dual_i5_E6.GC5-52L-NHW.Trinity.fasta /labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly/trinity_trim_NS.1157.004.NEBNext_dual_i7_F3---NEBNext_dual_i5_F3.GC9-67M-NHW.Trinity.fasta > cat_transcripts.fasta

