#!/bin/bash
#SBATCH --job-name=cluster
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=alyssa.ferreira@uconn.edu
#SBATCH -o cluster_%j.out
#SBATCH -e cluster_%j.err
#SBATCH --qos=general


module load vsearch
vsearch --cluster_fast ../usearch/cat_transcripts.fasta --centroids centroids_combined_cat_df_trans.fasta --uc combined_cat_df_trans.uc --id 0.95 --threads 12
