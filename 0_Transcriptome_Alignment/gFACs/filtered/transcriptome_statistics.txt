STATS:
Number of genes:	52483
Number of monoexonic genes:	14074
Number of multiexonic genes:	38409

Number of positive strand genes:	29194
Monoexonic:	8341
Multiexonic:	20853

Number of negative strand genes:	23289
Monoexonic:	5733
Multiexonic:	17556

Average overall gene size:	27489.082
Median overall gene size:	2019
Average overall CDS size:	944.147
Median overall CDS size:	708
Average overall exon size:	224.726
Median overall exon size:	133

Average size of monoexonic genes:	631.406
Median size of monoexonic genes:	438
Largest monoexonic gene:	5721
Smallest monoexonic gene:	300

Average size of multiexonic genes:	37330.393
Median size of multiexonic genes:	4636.5
Largest multiexonic gene:	940854
Smallest multiexonic gene:	153

Average size of multiexonic CDS:	1058.743
Median size of multiexonic CDS:	837
Largest multiexonic CDS:	14457
Smallest multiexonic CDS:	300

Average size of multiexonic exons:	196.999
Median size of multiexonic exons:	125
Average size of multiexonic introns:	8291.902
Median size of multiexonic introns:	239

Average number of exons per multiexonic gene:	5.374
Median number of exons per multiexonic gene:	4
Largest multiexonic exon:	8022
Smallest multiexonic exon:	9
Most exons in one gene:	79

Average number of introns per multiexonic gene:	4.374
Median number of introns per multiexonic gene:	3
Largest intron:	778429
Smallest intron:	9

The following columns do not involve codons:
Number of complete models:	52480
Number of 5' only incomplete models:	2
Number of 3' only incomplete models:	1
Number of 5' and 3' incomplete models:	0
