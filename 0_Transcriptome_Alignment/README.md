### Process for Transcriptome Alignment

## Initial Files: 
	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/Genes/Illumina/illumina_transcrips.fasta
	/labs/Wegrzyn/CoAdapTree_Douglasfir/Assembly_comparison/01_De_novo_transcriptome/02_Vsearch/all.95.centroids.80.centroids.uniq.pep.fasta
	/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly/trinity_trim_NS.1157.004.NEBNext_dual_i7_E3---NEBNext_dual_i5_E3.GC1-63L-NC.Trinity.fasta
	/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly/trinity_trim_NS.1170.003.NEBNext_dual_i7_H3---NEBNext_dual_i5_H3.GC7-65T-NC.Trinity.fasta
	/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly/trinity_trim_NS.1165.001.NEBNext_dual_i7_E6---NEBNext_dual_i5_E6.GC5-52L-NHW.Trinity.fasta
	/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly/trinity_trim_NS.1157.004.NEBNext_dual_i7_F3---NEBNext_dual_i5_F3.GC9-67M-NHW.Trinity.fasta

## Analysis conducted here:
	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment

## Process of Aligning Transcriptome

All scripts can be found in /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/scripts

1. Concatenated all transcripts togeter into cat_transcripts.fasta using the script cat.sh

2. Clustering was run using vsearch was run using vsearch.sh.

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/vsearch

3. After clustering gmap was run using gmap.sh

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/gmap

## Assessing Transcriptome Alignment Model

1. gFACs statistics on the initial annotation were generated using the script unfiltered_gFACs.sh 

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/gFACs/unfiltered

2. gFACs was used to filter the annotation using the script filtered_gFACs.sh with the following parameters

	--unique-genes-only \
	--min-CDS-size 300 \
	--rem-genes-without-start-and-stop-codon \
	--allowed-inframe-stop-codons 0 \
	--min-exon-size 9 \
	--min-intron-size 9 \

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/gFACs/filtered

2. BUSCO completeness score generated (on filtered annotation) by script busco.sh

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/busco

Results Summary:

	C:64.6%[S:41.6%,D:23.0%],F:5.6%,M:29.8%,n:1614
        1043    Complete BUSCOs (C)
        672     Complete and single-copy BUSCOs (S)
        371     Complete and duplicated BUSCOs (D)
        91      Fragmented BUSCOs (F)
        480     Missing BUSCOs (M)
        1614    Total BUSCO groups searched

3. Functional annotation using ENTAP, aligining to two databases, at the 50/50 and 80/80 query coverage

Databases used:

	/labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd
	/isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd 

Results located here: 
	
	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/entap_5050
	/labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/entap_8080

4. A table of the distribution of intron_lengths was made with the script intron_length.py and intron_length.sh and a table of gene lengths was made with the length.py and length.sh.

	Results located here: /labs/Wegrzyn/ConiferGenomes/Psme/analysis/SURF_annotation/final/0_Transcriptome_Alignment/violin_length
